# NestJS

---
## ⚒ Comandos útiles.
<br>

> Crear un nuevo proyecto
```
nest new <projectName>
```

> Crear un módulo
```
nest g mo <moduleName>
```